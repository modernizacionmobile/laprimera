//
//  DetailViewController.m
//  laPrimera
//
//  Created by MoWeMa on 2/7/16.
//  Copyright © 2016 MoWeMa. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()

//@property (nonatomic, readwrite) NSString *infos;
@end

@implementation DetailViewController

//@synthesize title;
@synthesize detailElement;

//@synthesize bajada = _bajada;

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"estoy en el detalle");
    // Do any additional setup after loading the view.
    //UIImage *imagen = [imag]
    [_detailTitle setText:titulo];
    [_detailInfo setText:bajada];
    [_detailImg setImage:imagen];
    
    //NSLog(@"cargó el detalle %@ y %@", titulo, _elemento);
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setTitle:(NSString *)text{
    titulo = text;
    NSLog(@"Segundo paso por setTitle %@", text);
}

- (void)setInfo:(NSString *)text{
    bajada = text;
    NSLog(@"paso por setInfo %@", text);
}

- (void)setImage:(UIImage *)img{
    imagen = img;
    NSLog(@"paso por setImage %@", img);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
