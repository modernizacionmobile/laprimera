//
//  MainViewController.h
//  laPrimera
//
//  Created by MoWeMa on 5/4/16.
//  Copyright © 2016 MoWeMa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;

@end
