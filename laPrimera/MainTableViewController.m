//
//  MainTableViewController.m
//  laPrimera
//
//  Created by MoWeMa on 2/7/16.
//  Copyright © 2016 MoWeMa. All rights reserved.
//

#import "MainTableViewController.h"
#import "DetailViewController.h"

@interface MainTableViewController ()

@end

@implementation MainTableViewController

@synthesize myTableView;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.myTableView.delegate = self; //added
    self.myTableView.dataSource = self; //added
    NSLog(@"estoy en la tabla");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSArray *)data {
    if (!_data){
        _data = [[NSArray alloc] init];
        
        /*
        _data = [[NSMutableDictionary alloc] init];
        
        NSArray *items = [NSArray arrayWithObjects:@"Debian",@"OSX",@"Ubuntu",@"Windows",nil];
        NSArray *descripciones = [NSArray arrayWithObjects:@"Robusto",@"Hermoso",@"Sencillo",@"Problemático",nil];
        [_data setObject:items forKey:@"Titulo"];
        [_data setObject:descripciones forKey:@"Bajada"];
        */
        NSURL *url = [[NSBundle mainBundle] URLForResource:@"SistemasOperativos" withExtension:@"plist"];
        NSArray *mainArray = [[NSArray alloc] initWithContentsOfURL:url];
        _data = mainArray;
        
        
        /*
        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
        formatter.numberStyle = NSNumberFormatterSpellOutStyle;
        
        for (int i = 0; i < 30; i++) {
            NSNumber *thisNumber = [NSNumber numberWithInt:i];
            NSString *spelledOutNumber = [formatter stringFromNumber:thisNumber];
            [_data addObject:spelledOutNumber];
        }*/
    }
    return _data;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSArray *items = self.data;
    
    return items.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    NSLog(@"configuro celda");

    // Configure the cell...
    NSDictionary *elemento = [self.data objectAtIndex:indexPath.row];
    cell.textLabel.text = [elemento objectForKey:@"Titulo" ];
    
    return cell;
}


 - (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"miSegue"])
    {
        NSLog(@"Primero Tomo Segue");
        
        //if you need to pass data to the next controller do it here
        
        NSIndexPath *indexPath = nil;
        NSDictionary *element = nil;
        
        indexPath = [myTableView indexPathForSelectedRow];
        
       // UIViewController *vcToPushTo = segue.destinationViewController;
        
        element = [_data objectAtIndex:indexPath.row];
        
        [[segue destinationViewController] setTitle:[element objectForKey:@"Titulo"]];
        [[segue destinationViewController] setInfo:[element objectForKey:@"Info"]];
        [[segue destinationViewController] setImage: [UIImage imageNamed:[element objectForKey:@"Imagen"]]];
         
        //vcToPushTo.title = element;
        //vcToPushTo.title = [element objectForKey:@"Info"];
        
        //NSLog(@"Obtengo este array %@", [elemento objectForKey:@"Titulo"] );
        
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Tercero, seleccioné: %@", [tableView cellForRowAtIndexPath:indexPath].textLabel.text);
    
    //DetailViewController *detailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"detalle"];
    
    //NSString *eltitulo = [tableView cellForRowAtIndexPath:indexPath].textLabel.text;
    
    //[detailViewController setTitle:eltitulo];
    

    /*
    
         
     NSString *title = [tableView cellForRowAtIndexPath:indexPath].textLabel.text;
     self.title = title;
     [tableView deselectRowAtIndexPath:indexPath animated:YES];

 */
}

@end
