//
//  MainTableViewController.h
//  laPrimera
//
//  Created by MoWeMa on 2/7/16.
//  Copyright © 2016 MoWeMa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainTableViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSArray *data;
@property (strong, nonatomic) IBOutlet UITableView *myTableView;

@end
