//
//  DetailViewController.h
//  laPrimera
//
//  Created by MoWeMa on 2/7/16.
//  Copyright © 2016 MoWeMa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController
{
    NSString *titulo;
    NSString *bajada;
    UIImage *imagen;
    NSDictionary *elemento;
}

@property (strong, nonatomic) IBOutlet UIView *detailView;
@property (weak, nonatomic) IBOutlet UILabel *detailTitle;
@property (weak, nonatomic) IBOutlet UILabel *detailInfo;
@property (weak, nonatomic) IBOutlet UIImageView *detailImg;
@property (strong, nonatomic) NSDictionary *detailElement;

- (void)setTitle:(NSString *)text;

- (void)setInfo:(NSString *)text;

- (void)setImage:(UIImage *)img;

@end
